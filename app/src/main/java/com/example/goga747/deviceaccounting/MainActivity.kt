package com.example.goga747.deviceaccounting


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.AppBarLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.ImageView
import com.example.goga747.deviceaccounting.db.DBHelper
import com.example.goga747.deviceaccounting.model.Device
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.gson.Gson
import com.mikepenz.materialdrawer.AccountHeader
import com.mikepenz.materialdrawer.AccountHeaderBuilder
import com.mikepenz.materialdrawer.Drawer
import com.mikepenz.materialdrawer.DrawerBuilder
import com.mikepenz.materialdrawer.model.*
import com.mikepenz.materialdrawer.util.DrawerImageLoader
import com.squareup.picasso.Picasso
import com.vansuita.pickimage.bean.PickResult
import com.vansuita.pickimage.bundle.PickSetup
import com.vansuita.pickimage.dialog.PickImageDialog
import com.vansuita.pickimage.listeners.IPickCancel
import com.vansuita.pickimage.listeners.IPickResult
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),

        AuthenticationUserFragment.OnAuthenticationUserFragmentListener,
        CreateNewDeviceFragment.OnFragmentCreateNewDeviceListener,
        DisplayListTakenDevicesFragment.OnFragmentDisplayListTakenDevicesInteractionListener,
        OnFragmentListener,
        IPickResult {

    override fun showAppBar(flag: Boolean) {
        displayAppBar(flag)
    }

    override fun getExternalKeyDeviceForDisplayFullData(key: String) {
        val bundle = Bundle()
        bundle.putString(getString(R.string.key_bundle_external_key_device), key)
        displayFragment(getString(R.string.key_display_full_device_usage_history_fragment), bundle = bundle)
    }

    override fun chooseImageManager() {
        PickImageDialog.build(PickSetup(), this).setOnPickCancel(object : IPickCancel {
            override fun onCancelClick() {
                pushLog("onPickResult", "click button Cancel")
                //
            }
        }).show(this)
    }

    override fun onPickResult(result: PickResult) {
        pushLog("onPickResult", result)
        if (result.error == null) {
            displayCreateNewDeviceFragment(result.uri)
        } else {
            pushLog("onPickResult", result.error)
        }

    }

    override fun closeMe(keyFragment: String) {
        pushLog("closeMe", keyFragment)
        closeFragment(keyFragment)
        if (keyFragment == getString(R.string.key_pick_up_device_fragment)) {
            displayMainFragment(this.mainFragment, Unit)
        }
//        displayMainFragment(this.mainFragment,closeFragment(keyFragment))
    }

    private var newDevice: Device? = null
    private var mAuth: FirebaseAuth? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private val mainFragment: Fragment = DisplayListTakenDevicesFragment()

    private fun displayCreateNewDeviceFragment(imageDeviceUri: Uri? = Uri.EMPTY) {
        if (this.newDevice != null) {
            val bundle = Bundle().apply {
                putString(getString(R.string.key_bundle_new_device_record), Gson().toJson(this@MainActivity.newDevice).toString())
                putString(getString(R.string.key_bundle_uri_image_device), imageDeviceUri.toString())
            }


            displayFragment(getString(R.string.key_create_new_device_fragment), bundle = bundle)
        } else {
            pushLog("displayCreateNewDeviceFragment "
                    , "Can't display VerifyingFragment because this.newDevice == null")
        }

    }

    override fun getStatusAuthenticationUser(statusAuthorized: Boolean) {
        closeFragment(getString(R.string.key_login_user_fragment))
        if (statusAuthorized) {
            displayBarcodeActivity(REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE)
        } else {
            pushLog("getStatusAuthenticationUser statusAuthorized =", false)
        }

    }

    private val REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE: Int = 5005
    private val REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE: Int = 6006
    private val REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE: Int = 5555
    private val REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER: Int = 7007

    private fun displayBarcodeActivity(requestCode: Int) {
        startActivityForResult(Intent(applicationContext, BarcodeActivity::class.java), requestCode)
    }

    private fun Unit.displayBarcodeActivity(requestCode: Int) {
        pushLog(" Unit.displayBarcodeActivity", "1111111111111")
        startActivityForResult(Intent(applicationContext, BarcodeActivity::class.java), requestCode)
    }

    private fun startFirebaseAuth(requestCode: Int) {
        mGoogleSignInClient = GoogleSignIn.getClient(this, getGoogleSignInOptions()!!)
        mAuth = FirebaseAuth.getInstance()
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, requestCode)
    }

    private fun getGoogleSignInOptions(): GoogleSignInOptions? {
        return GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()
    }

    private fun signOut() {
        if (accountHeader != null){
            updateProfileInAccountHeader(false)
        }
        val mAuth = FirebaseAuth.getInstance()
        mAuth.signOut()
        val mGoogleSignInClient = GoogleSignIn.getClient(this, getGoogleSignInOptions()!!)
        mGoogleSignInClient.signOut()

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && data != null) {
            when (requestCode) {
                this.REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE -> {
                    val dataBarcodeString = data.getStringExtra(getString(R.string.key_bundle_record_device))
                    this.newDevice = Gson().fromJson(dataBarcodeString, Device::class.java)

                    displayCreateNewDeviceFragment()
                }
                this.REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE -> {

                    val dataBarcodeString = data.getStringExtra(
                            getString(R.string.key_bundle_record_device))
                    val device: Device = Gson().fromJson(dataBarcodeString, Device::class.java)
                    val bundle = Bundle().apply {
                        putString(getString(R.string.key_bundle_record_device)
                                , Gson().toJson(device).toString())
                    }


                    displayFragment(getString(R.string.key_pick_up_device_fragment), bundle = bundle)

                    pushLog("onActivityResult RESULT_OK" +
                            " REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE", dataBarcodeString)
                }
                this.REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE -> {
                    pushLog("onActivityResult RESULT_OK",
                            "REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE")
                    dataProcessingGoogleSingIn(data, REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE)
                    displayBarcodeActivity(REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE)
                }
                this.REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER -> {
                    pushLog("onActivityResult RESULT_OK",
                            "REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER")
                    dataProcessingGoogleSingIn(data, REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER)
                    displayFragment(getString(R.string.key_create_new_user_fragment))
                }
            }

        } else if (resultCode == Activity.RESULT_CANCELED) {
            pushLog("onActivityResult RESULT_CANCELED", "")
            when (requestCode) {
                this.REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE -> {
                    pushLog("onActivityResult RESULT_CANCELED" +
                            " REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE", "")
                }
                this.REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE -> {
                    val db: DBHelper = DBHelper.newInstance(applicationContext, "DeviceAccounting", null, 1)
                    db.deleteSession()
                    displayMainFragment(this.mainFragment,null)
                    pushLog("onActivityResult RESULT_CANCELED" +
                            " REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE", "")
                }
                this.REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE -> {
                    pushLog("onActivityResult RESULT_CANCELED",
                            "REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE"
                                    + "Permission denied")
                }
                this.REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER -> {
                    pushLog("onActivityResult RESULT_CANCELED",
                            "REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER"
                                    + "Permission denied")
                }
            }
        }

    }

    private fun dataProcessingGoogleSingIn(data: Intent?, thenOpen: Int) {
        val task = GoogleSignIn.getSignedInAccountFromIntent(data)
        try {
            val account = task.getResult(ApiException::class.java)
            firebaseAuthWithGoogle(account)
            updateProfileInAccountHeader(true)
//                    .displayBarcodeActivity(thenOpen)
            pushLog("onActivityResult", "success authenticate google")
        } catch (e: ApiException) {
            pushLog("onActivityResult ApiException", "Google sign in failed  $e")
            signOut()

        }
    }

    private fun updateProfileInAccountHeader(newAccount:Boolean) {
        accountHeader!!.clear()
        if (newAccount){
            accountHeader?.addProfile(getProfileCurrentUser(), 0)
        } else {
            accountHeader?.addProfile(getDefaultProfileDrawerItem(), 0)
        }

    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)

        mAuth!!.signInWithCredential(credential)
                .addOnCompleteListener(this, { task: Task<AuthResult> ->
                    if (task.isSuccessful) {
                        pushLog("firebaseAuthWithGoogle", "signInWithCredential:success")

                    } else {
                        pushLog("firebaseAuthWithGoogle", "signInWithCredential:failure"
                                + task.exception)

                    }
                })
    }



    private var visibleFragment: Fragment? = null

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = MainActivity::class.java.simpleName


    private fun displayAppBar(flag: Boolean) {
//        pushLog("displayAppBar", flag)
//        if (flag) {
//            app_bar_layout_main.visibility = AppBarLayout.VISIBLE
//        } else {
//            app_bar_layout_main.visibility = AppBarLayout.GONE
//        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        displayMainFragment(this.mainFragment, null)
        initDrawerImageLoader()
        initNavigationDrawer()
    }

    private fun initDrawerImageLoader() {
        DrawerImageLoader.init(object : DrawerImageLoader.IDrawerImageLoader {
            override fun placeholder(ctx: Context?): Drawable {
                return resources.getDrawable(R.drawable.ic_placeholder_drawer_image_loader)

            }

            override fun placeholder(ctx: Context?, tag: String?): Drawable {

                return resources.getDrawable(R.drawable.ic_placeholder_drawer_image_loader)
            }

            override fun set(imageView: ImageView?, uri: Uri?, placeholder: Drawable?) {
                Picasso.with(imageView!!.context).load(uri).placeholder(placeholder).into(imageView)
            }

            override fun set(imageView: ImageView?, uri: Uri?, placeholder: Drawable?, tag: String?) {
                Picasso.with(imageView!!.context).load(uri).placeholder(placeholder).into(imageView)
            }

            override fun cancel(imageView: ImageView?) {
                Picasso.with(imageView!!.context).cancelRequest(imageView)
            }
        })
    }

    private fun initNavigationDrawer() {
        val itemDisplayListTakenDevice = PrimaryDrawerItem()
                .withIdentifier(R.integer.id_item_display_list_taken_device.toLong())
                .withName(R.string.action_display_list_taken_devices)
                .withIcon(R.drawable.ic_action_list_taken_devices)
                .withSelectable(false)
        val itemPickUpDevice = PrimaryDrawerItem()
                .withIdentifier(R.integer.id_item_pick_up_device.toLong())
                .withName(R.string.action_pick_up_device)
                .withIcon(R.drawable.ic_action_pick_up_device)
                .withSelectable(false)
        val itemAddDevice = PrimaryDrawerItem()
                .withIdentifier(R.integer.id_item_add_device.toLong())
                .withName(R.string.action_add_device)
                .withIcon(R.drawable.ic_action_add_device)
                .withSelectable(false)
        val itemCreateNewUserFragment = PrimaryDrawerItem()
                .withIdentifier(R.integer.id_item_create_new_user.toLong())
                .withName(R.string.action_create_new_user)
                .withIcon(R.drawable.ic_action_add_new_user)
                .withSelectable(false)
        val itemSingOut = PrimaryDrawerItem()
                .withIdentifier(R.integer.id_item_sing_out.toLong())
                .withName("Sing Out Admin")
                .withIcon(R.drawable.ic_action_sing_out_admin)
                .withSelectable(false)



        accountHeader = getAccountHeaderNavigation()

        if (isGoogleSingIn()) {
            accountHeader!!.addProfile(getProfileCurrentUser(), 0)
        } else {
            accountHeader!!.addProfile(getDefaultProfileDrawerItem(), 0)
        }
        drawer = DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(accountHeader!!)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        itemDisplayListTakenDevice,
                        itemPickUpDevice,
                        SectionDrawerItem().withName("Admin"),
                        itemAddDevice,
                        itemCreateNewUserFragment,
                        itemSingOut
                )
                .withOnDrawerItemClickListener({ view, position, drawerItem ->
                    when (drawerItem.identifier.toInt()) {
                        R.integer.id_item_display_list_taken_device -> {
                            if (this.visibleFragment != this.mainFragment) {
                                displayMainFragment(this.mainFragment, closeFragment())
                            } else {
                                displayMainFragment(this.mainFragment, null)
                            }
                        }
                        R.integer.id_item_pick_up_device -> {
                            displayAppBar(false)
                            if (checkSessionUser()) {
                                displayBarcodeActivity(REQUEST_CODE_BARCODE_ACTIVITY_PICK_UP_DEVICE)
                            } else {
                                displayFragment(getString(R.string.key_login_user_fragment))
                            }
                        }
                        R.integer.id_item_add_device -> {
                            if (isFirebaseAuth()) {
                                displayBarcodeActivity(REQUEST_CODE_BARCODE_ACTIVITY_ADD_DEVICE)
                            } else {
                                startFirebaseAuth(REQUEST_CODE_AUTHENTICATION_ACTIVITY_ADD_DEVICE)
                            }

                        }
                        R.integer.id_item_create_new_user -> {
                            if (isFirebaseAuth()) {
                                displayFragment(getString(R.string.key_create_new_user_fragment))
                            } else {
                                startFirebaseAuth(REQUEST_CODE_AUTHENTICATION_ACTIVITY_CREATE_NEW_USER)
                            }


                        }
                        R.integer.id_item_sing_out -> {
                            if (isFirebaseAuth()) {
                                signOut()
                            }
                        }

                    }

                    false
                })
                .build()

    }

    private fun getDefaultProfileDrawerItem() = ProfileDrawerItem().withName("Anonymous user")

    private fun getProfileCurrentUser(): ProfileDrawerItem {
        pushLog("getProfileCurrentUser","getProfileCurrentUser")
        val personName: String
        val personEmail: String
        val personPhoto: String
        val acct = GoogleSignIn.getLastSignedInAccount(applicationContext)
        if (acct != null) {
            pushLog("getProfileCurrentUser","acct != null")
              personName = acct.displayName!!
              personEmail = acct.email!!
              personPhoto = acct.photoUrl.toString()
        } else return getDefaultProfileDrawerItem()
        return ProfileDrawerItem()
                .withName(personName)
                .withEmail(personEmail)
                .withIcon(personPhoto)
    }

    private fun getAccountHeaderNavigation(): AccountHeader? {
        return if (accountHeader != null) {
            accountHeader
        } else {
            AccountHeaderBuilder()
                    .withActivity(this)
                    .withHeaderBackground(R.drawable.background_navigator)
                    .build()
        }
    }

    var accountHeader: AccountHeader? = null
    var drawer: Drawer? = null

    override fun onBackPressed() {
        if (drawer!!.isDrawerOpen) {
            drawer!!.closeDrawer()
        } else {
            super.onBackPressed()
        }
    }

    private fun displayMainFragment(mainFragment: Fragment, closeFragment: Unit?) {
        val newMainFragment = mainFragment
        pushLog("displayMainFragment ${this.visibleFragment.toString()}  ", mainFragment)
        if (this.visibleFragment != mainFragment) {
            pushLog("displayMainFragmentthis.visibleFragment!! != s ", true)
            val transaction = supportFragmentManager.beginTransaction()
            pushLog("displayMainFragment newMainFragment", newMainFragment.toString())
            visibleFragment = newMainFragment
            transaction.replace(R.id.fragment_frame_main_activity, visibleFragment)
            transaction.commit()
        }
        displayAppBar(true)


    }

    private fun displayFragment(keyFragment: String, bundle: Bundle? = null) {
        val transaction = supportFragmentManager.beginTransaction()
        pushLog("displayFragment $keyFragment", this!!.visibleFragment.toString())
        when (keyFragment) {
            getString(R.string.key_login_user_fragment) -> {
                displayAppBar(false)
                this.visibleFragment = AuthenticationUserFragment()
            }
            getString(R.string.key_create_new_device_fragment) -> {
                displayAppBar(false)
                this.visibleFragment = CreateNewDeviceFragment()
                transaction.addToBackStack(getString(R.string.key_create_new_device_fragment))
            }

            getString(R.string.key_pick_up_device_fragment) -> {
                displayAppBar(false)
                this.visibleFragment = PickUpDeviceFragment()
//                transaction.addToBackStack(getString(R.string.key_pick_up_device_fragment))
            }
            getString(R.string.key_create_new_user_fragment) -> {
                displayAppBar(false)
                this.visibleFragment = CreateNewUserFragment()
                transaction.addToBackStack(getString(R.string.key_create_new_user_fragment))
            }
            getString(R.string.key_display_full_device_usage_history_fragment) -> {
                displayAppBar(false)
                this.visibleFragment = DisplayFullDeviceUsageHistoryFragment()
                transaction.addToBackStack(
                        getString(R.string.key_display_full_device_usage_history_fragment))
            }
        }

        visibleFragment!!.arguments = bundle
        transaction.replace(R.id.fragment_frame_main_activity, visibleFragment, keyFragment)
        transaction.commit()
    }

    private fun closeFragment(keyFragment: String = "") {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.remove(this.visibleFragment).commit()

        val backStackEntry = supportFragmentManager.backStackEntryCount
        if (backStackEntry > 0) {
            for (i in 0 until backStackEntry) {
                supportFragmentManager.popBackStackImmediate()
            }
        }

//        displayMainFragment(getString(R.string.key_display_list_taken_devices_fragment), closeFragment(keyFragment))
        pushLog("closeFragment", keyFragment)
    }

    private fun pushLog(topic: String, message: Any) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }



    private fun isFirebaseAuth() = FirebaseAuth.getInstance().currentUser != null
    private fun isGoogleSingIn(): Boolean =
            GoogleSignIn.getLastSignedInAccount(applicationContext) != null

    private fun checkSessionUser(): Boolean {
        val db: DBHelper = DBHelper.newInstance(applicationContext, getString(R.string.name_date_base), null, 1)
        // TODO () В идеале db.getCurrentSession(db.writableDatabase).count == 1 else if count > 1 delete
        return db.getCurrentSession(db.writableDatabase).count > 0

    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        this.newDevice?.let {
            outState.putString(getString(R.string.key_bundle_record_new_device),
                    Gson().toJson(it).toString())
        }

    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        if (savedInstanceState.containsKey(getString(R.string.key_bundle_record_new_device))) {
            this.newDevice = Gson().fromJson(savedInstanceState
                    .getString(getString(R.string.key_bundle_record_new_device)),
                    Device::class.java)
        }

    }
}




