package com.example.goga747.deviceaccounting.db

import android.provider.BaseColumns

/**
 * Created by goga747 on 03.05.2018.
 */
class ListContract {

    companion object {
        val TABLE_USER_SESSION: String = "user_session"
        class UserSessionEntry : BaseColumns{
            companion object {
                val KEY_USER = "user"
                val KEY_USER_KEY_FB = "user_key_fb"
                val KEY_CREATED = "created"
            }

        }
        val DEVICES_NODE: String = "Devices"
        val USERS_NODE: String = "Users"
        val TAKEN_DEVICES_NODE: String = "TakenDevices"

        val  SUB_DEVICE_NODE: String = "Device"
        val  SUB_TAKEN_NODE: String = "Taken"
        val  STORAGE_DEVICE_PHOTOS = "device_photos"
        class KeysDevicesForFairBaseNode : BaseColumns{
            companion object {
                val KEY_EXTERNAL_KEY = "externalKey"

            }

        }
        class KeysUsersForFairBaseNode : BaseColumns{
            companion object {
                val KEY_LOGIN = "login"

            }

        }
    }

}