package com.example.goga747.deviceaccounting

import android.content.Context
import android.support.v4.app.Fragment
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.goga747.deviceaccounting.db.DBHelper
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.User
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_authentication_user.*
import java.io.Serializable
import java.util.*

/**
 * A placeholder fragment containing a simple view.
 */
class AuthenticationUserFragment : Fragment() {

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = AuthenticationUserFragment::class.java.simpleName
    private var mListenerAuthenticationUser: OnAuthenticationUserFragmentListener? = null
    private var myRefUsersNode: DatabaseReference? = null
    private var valueEventListener: ValueEventListener? = null

    interface OnAuthenticationUserFragmentListener:OnFragmentListener {
        // TODO () подумай передать и request code
        fun getStatusAuthenticationUser(statusAuthorized: Boolean)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_login_fragment_login.setOnClickListener({

            if (!checkDataDisplay()) {
                Toast.makeText(context, "Not all fields are filled", Toast.LENGTH_SHORT).show()
            } else {


                myRefUsersNode = FirebaseDatabase.getInstance().reference.child(ListContract.USERS_NODE)

                // TODO () как бы данные не пропали
                val login = input_login_fragment_login.text.toString()
                val loginByteArray = login.toByteArray()
                val passwordByteArray = Base64.encode(input_password_fragment_login.text.toString()
                        .toByteArray(), Base64.DEFAULT)
                pushLog("LOGIN",login)
                pushLog("PASSWORD",input_password_fragment_login.text.toString())
                val checkKey = UUID.nameUUIDFromBytes(passwordByteArray + loginByteArray)
                pushLog("checkKey",checkKey.toString())
                valueEventListener = myRefUsersNode!!.orderByChild(ListContract.Companion.KeysUsersForFairBaseNode.KEY_LOGIN)
                        .equalTo(login)
                        .limitToFirst(1)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onCancelled(snapshot: DatabaseError?) {
                                pushLog("onCancelled", snapshot.toString())
                            }

                            override fun onDataChange(snapshot: DataSnapshot?) {

                                pushLog("onDataChange checkKey", checkKey.toString())
                                if (snapshot!!.value == null) {
                                    Toast.makeText(context, "login or password are incorrect"
                                            , Toast.LENGTH_SHORT).show()
                                    pushLog("onDataChange", "snapshot!!.value == null")
                                } else {

                                    var user: User? = null
                                    snapshot.children.forEach {
                                        user = it.getValue(User::class.java) as User
                                        user!!.key = it.key
                                    }
                                    user?.let {
                                        checkParameters(checkKey, user, login)
                                    }

                                }
                            }
                        })


            }
        })
    }

    private fun checkParameters(checkKey: UUID, user: User?, login: String) {
        this.myRefUsersNode!!.removeEventListener(this.valueEventListener)
        if (checkKey.toString() == user!!.token) {
            createSession(user)
        } else {
            pushLog("checkParameters", "checkKey.toString() != user.token")
            Toast.makeText(context,
                    "login or password are incorrect"
                    , Toast.LENGTH_SHORT).show()
        }
    }

    private fun createSession(user: User) {
        val db = DBHelper.newInstance(activity!!.applicationContext
                , getString(R.string.name_date_base), null, 1)
        db.addSession(user.login, user.key)
        pushLog("onDataChange", "checkKey.toString() == user.token")
        mListenerAuthenticationUser!!
                .getStatusAuthenticationUser(true)
    }

    private fun checkDataDisplay(): Boolean = !((input_login_fragment_login.text.toString()
            .replace(" ", "") == "")
            or (input_password_fragment_login.text.toString()
            .replace(" ", "") == ""))

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnAuthenticationUserFragmentListener) {
            mListenerAuthenticationUser = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement " +
                    "OnAuthenticationUserFragmentListener")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_authentication_user, container, false)
    }

    override fun onDetach() {
        super.onDetach()
        this.mListenerAuthenticationUser!!.showAppBar(true)
        this.mListenerAuthenticationUser = null
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }
}
