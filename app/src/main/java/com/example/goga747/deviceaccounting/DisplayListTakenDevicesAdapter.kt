package com.example.goga747.deviceaccounting

import android.content.Context
import android.graphics.Bitmap
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.goga747.deviceaccounting.model.DeviceTakenDevice
import com.example.goga747.deviceaccounting.model.UserTakenDevice
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_taken_device.view.*

import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by goga747 on 02.05.2018.
 */
class DisplayListTakenDevicesAdapter(var data: MutableList<HashMap<DeviceTakenDevice, UserTakenDevice?>>?
                                     , var mListener: OnAdapterDisplayListTakenDevicesListener
                                     , var month: Array<String>
                                     , var context: Context)
    : RecyclerView.Adapter<DisplayListTakenDevicesAdapter.MyViewHolder>() {

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = DisplayListTakenDevicesAdapter::class.java.simpleName

    interface OnAdapterDisplayListTakenDevicesListener {
        fun getExternalKeySelectedDevice(externalKey: String)
    }

    fun updateData(data: MutableList<HashMap<DeviceTakenDevice, UserTakenDevice?>>?) {
        pushLog("updateData", "updateData")

        this.data = data!!
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return DisplayListTakenDevicesAdapter.MyViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_taken_device, parent, false))
    }

    override fun getItemCount(): Int = data!!.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        pushLog("onBindViewHolder", "onBindViewHolder")
        if (data?.get(position)?.isNotEmpty()!!) {
            pushLog("onBindViewHolder", "data?.getExternalKeySelectedDevice(position)?.isNotEmpty()!!")
//            pushLog("onBindViewHolder", data?.get(position)!!)
            val infoDevice = data?.get(position)!!.keys.first()
            holder.model.text = infoDevice.model

            if (infoDevice.uriImage.isNotEmpty()) {
                pushLog("onBindViewHolder", "infoDevice.uriImage.isNotEmpty()")
                Picasso.with(context).load(infoDevice.uriImage)
                        .placeholder(R.mipmap.gallery_colored)
                        .error(android.R.mipmap.sym_def_app_icon)
                        .config(Bitmap.Config.RGB_565)
                        .fit()
                        .centerCrop()
                        .into(holder.image)
            }

            if (data?.get(position)!!.get(infoDevice) != null) {
                pushLog("onBindViewHolder", "data?.getExternalKeySelectedDevice(position)!!.values.isNotEmpty()")
                val infoUser = data?.get(position)!!.get(infoDevice)!!
                val infoText = infoUser.login +
                        " " + buildDisplayTime(getTimeString(infoUser.dateAssignment))
                holder.infoTakenText.text = infoText

            }
            holder.externalKey = infoDevice.externalKey
            holder.layout.setOnClickListener {
                pushLog("getExternalKeySelectedDevice", holder.externalKey)
                mListener.getExternalKeySelectedDevice(holder.externalKey)
            }
        }
    }

    private fun buildDisplayTime(timeString: String?): String {
        val fullDateExpirationString: String = timeString!!
        val tagSeparate = context.getString(R.string.tag_separate_date)
        val separateFullDateExpirationList = fullDateExpirationString.split((tagSeparate).toRegex())


        return separateFullDateExpirationList[1] +
                tagSeparate +
                month[separateFullDateExpirationList[0].toInt()-1] +
                " " +
                separateFullDateExpirationList[2] +
                tagSeparate +
                separateFullDateExpirationList[3]

    }

    private fun getTimeString(timeMillionsSecond: Long): String? {

        val date = Date(timeMillionsSecond)
        val formatter = SimpleDateFormat("M:d:HH:mm")
        return formatter.format(date)
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }

    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var image: ImageView = view.img_item_taken_device
        var model: TextView = view.text_model_item_taken_device
        var infoTakenText: TextView = view.text_info_taken_item_taken_device
        var layout: ConstraintLayout = view.layout_item_taken_device
        lateinit var externalKey: String
    }
}