package com.example.goga747.deviceaccounting

import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.TextureView
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.goga747.deviceaccounting.model.UserTakenDevice
import kotlinx.android.synthetic.main.item_device_info_use_history.view.*
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by goga747 on 10.05.2018.
 */
class DisplayFullDeviceUsageHistoryAdapter(var data: MutableList<UserTakenDevice>?
                                           , var tagSeparateDate: String
                                           , var month: Array<String>)
    : RecyclerView.Adapter<DisplayFullDeviceUsageHistoryAdapter.MyViewHolder>() {
    private val LOG_TAG = "GOT"
    private val LOG_HEAD = DisplayFullDeviceUsageHistoryAdapter::class.java.simpleName

    fun updateData(data: MutableList<UserTakenDevice>?) {
        this.data = data
        this.notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return DisplayFullDeviceUsageHistoryAdapter.MyViewHolder(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_device_info_use_history, parent, false))
    }

    override fun getItemCount(): Int = data!!.size

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (data!!.isNotEmpty()) {
            pushLog("onBindViewHolder", "data!!.isNotEmpty()")
            val infoUser = data!![position]

            val infoText = infoUser.login +
                    " " + buildDisplayTime(getTimeString(infoUser.dateAssignment))
            pushLog("onBindViewHolder", infoText)
            holder.textInfo!!.text = infoText
        }
    }

    private fun buildDisplayTime(timeString: String?): String {
        val fullDateExpirationString: String = timeString!!
        val tagSeparate = this.tagSeparateDate
        val separateFullDateExpirationList = fullDateExpirationString.split((tagSeparate).toRegex())


        return separateFullDateExpirationList[1] +
                tagSeparate +
                month[separateFullDateExpirationList[0].toInt()-1] +
                " " +
                separateFullDateExpirationList[2] +
                tagSeparate +
                separateFullDateExpirationList[3]

    }

    private fun getTimeString(timeMillionsSecond: Long): String? {

        val date = Date(timeMillionsSecond)
        val formatter = SimpleDateFormat("M:d:HH:mm")
        return formatter.format(date)
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }

    class MyViewHolder(var view: View) : RecyclerView.ViewHolder(view) {
        var textInfo: TextView? = view.text_device_info_use_history
    }
}