package com.example.goga747.deviceaccounting


import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.Device
import com.example.goga747.deviceaccounting.model.UserTakenDevice
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_display_full_device_usage_history.*


/**
 * A simple [Fragment] subclass.
 */
class DisplayFullDeviceUsageHistoryFragment : Fragment()
        , ChildEventListener {


    override fun onCancelled(error: DatabaseError?) {
        pushLog("onCancelled", error.toString())
    }

    override fun onChildMoved(snapshot: DataSnapshot?, p1: String?) {
        pushLog("onCancelled", snapshot.toString())
    }

    override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
        pushLog("onChildChanged", snapshot.toString())
    }

    override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {

        if (snapshot!!.hasChild(ListContract.SUB_DEVICE_NODE)) {
            if (snapshot!!.hasChild(ListContract.SUB_TAKEN_NODE)) {
                listDeviceUsageHistory!!.clear()
                snapshot!!.child(ListContract.SUB_TAKEN_NODE).children.forEach {
                    //

                    listDeviceUsageHistory!!
                            .add(it.getValue(UserTakenDevice::class.java)
                                    as UserTakenDevice)

                }

                updateDataAdapter()
            }
        }

    }

    override fun onChildRemoved(snapshot: DataSnapshot?) {
        pushLog("onChildRemoved", snapshot.toString())
    }

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = DisplayFullDeviceUsageHistoryFragment::class.java.simpleName
    private var mDisplayFullInfoListener: OnFragmentListener? = null

    private var listDeviceUsageHistory: MutableList<UserTakenDevice>? = null
    private var adapter: DisplayFullDeviceUsageHistoryAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (this.arguments != null &&
                this.arguments!!.containsKey(getString(R.string.key_bundle_external_key_device))) {
            listDeviceUsageHistory = mutableListOf()
            val externalKey = this.arguments!!
                    .getString(getString(R.string.key_bundle_external_key_device))
            requestDataDeviceUsageHistoryServer(externalKey)
            requestDataCurrentTakenDevice(externalKey)
            recycler_list_device_usage_history_fragment.layoutManager = LinearLayoutManager(
                    activity!!.applicationContext
                    , LinearLayout.VERTICAL
                    , false)
            adapter = DisplayFullDeviceUsageHistoryAdapter(listDeviceUsageHistory
                    , getString(R.string.tag_separate_date)
                    , activity!!.applicationContext.resources.getStringArray(R.array.month))
            recycler_list_device_usage_history_fragment.adapter = adapter
        } else {
            mDisplayFullInfoListener!!
                    .closeMe(getString(R.string.key_display_full_device_usage_history_fragment))
        }
    }

    private fun requestDataCurrentTakenDevice(externalKey: String?) {
        val myRef = FirebaseDatabase.getInstance().reference.child(ListContract.DEVICES_NODE)
        val query = myRef.orderByChild(ListContract
                .Companion.KeysDevicesForFairBaseNode.KEY_EXTERNAL_KEY)
                .equalTo(externalKey)
                .limitToFirst(1)
        query.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(error: DatabaseError?) {
                pushLog("onCancelled Device", error.toString())
            }

            override fun onDataChange(snapshot: DataSnapshot?) {
                if (snapshot!!.value != null) {
                    snapshot.children.forEach {
                        val currentTakenDevice: Device = it.getValue(Device::class.java)
                                as Device
                        displayCurrentTakenDeviceInfo(currentTakenDevice)
                    }
                }
                query.removeEventListener(this)
            }
        })
    }

    private fun requestDataDeviceUsageHistoryServer(externalKey: String) {
        val myRef = FirebaseDatabase.getInstance().reference.child(ListContract.TAKEN_DEVICES_NODE)
        val query = myRef.orderByChild(ListContract.SUB_DEVICE_NODE
                + getString(R.string.tag_separate_query_firebase)
                + ListContract.Companion.KeysDevicesForFairBaseNode.KEY_EXTERNAL_KEY)
                .equalTo(externalKey)
                .limitToFirst(1)
        query.addChildEventListener(this)
    }

    private fun updateDataAdapter() {
        adapter!!.updateData(listDeviceUsageHistory!!.apply { sortBy { it.dateAssignment } })
    }

    private fun displayCurrentTakenDeviceInfo(device: Device) {


        text_model_device_usage_history_fragment.text = device.model
        text_os_device_usage_history_fragment.text = device.os
        text_ram_device_usage_history_fragment.text = device.ram
        text_screen_resolution_device_usage_history_fragment.text = "${device.screenDensity}" +
                " ${device.screenResolution}"
        text_user_device_usage_history_fragment.text = device.user
        text_pk_device_usage_history_fragment.text = device.externalKey
        if (isImg(device.uriImage)) {

            Picasso.with(context).load(device.uriImage)
                    .placeholder(android.R.mipmap.sym_def_app_icon)
                    .error(android.R.mipmap.sym_def_app_icon)
                    .config(Bitmap.Config.RGB_565)
                    .fit()
                    .centerCrop()
                    .into(image_view_device_usage_history_fragment)
        }

    }

    private fun isImg(uriImage: String) = !uriImage.equals(Uri.EMPTY) && uriImage != ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_display_full_device_usage_history, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            mDisplayFullInfoListener = context
        } else {
            throw RuntimeException(context!!.toString()
                    + " must implement OnFragmentListener for" +
                    "DisplayFullDeviceUsageHistoryFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()
        pushLog("onDetach","onDetach")
        this.mDisplayFullInfoListener!!.showAppBar(true)
        mDisplayFullInfoListener = null
    }

    private fun pushLog(topic: String, message: Any) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }
}// Required empty public constructor
