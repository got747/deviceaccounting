package com.example.goga747.deviceaccounting.model

/**
 * Created by goga747 on 25.04.2018.
 */
open class Device () {

    lateinit var model: String
    lateinit var os: String
    lateinit var ram: String
    lateinit var screenResolution: String
    lateinit var screenDensity: String
    lateinit var user: String
    lateinit var externalKey: String
    lateinit var userCreator: String
    lateinit var uriImage: String

    constructor(model: String, os: String, ram: String, screenResolution: String, screenDensity: String, user: String, externalKey: String, userCreator: String, uriImage: String) : this() {
        this.model = model
        this.os = os
        this.ram = ram
        this.screenResolution = screenResolution
        this.screenDensity = screenDensity
        this.user = user
        this.externalKey = externalKey
        this.userCreator = userCreator
        this.uriImage = uriImage
    }

    override fun toString(): String = "\n"+
            this.model + "\n" +
            this.os + "\n" +
            this.ram + "\n" +
            this.screenResolution + "\n" +
            this.screenDensity + "\n" +
            this.user + "\n" +
            this.userCreator + "\n" +
            this.externalKey + "\n"

}

