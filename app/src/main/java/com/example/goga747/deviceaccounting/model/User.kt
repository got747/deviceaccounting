package com.example.goga747.deviceaccounting.model

/**
 * Created by goga747 on 03.05.2018.
 */
open class User() {
    var key: String = ""
    lateinit var login: String
    lateinit var token: String

    constructor(token: String,
                login: String) : this() {
        this.login = login
        this.token = token

    }

    constructor(key: String, login: String, token: String) : this() {
        this.key = key
        this.login = login
        this.token = token
    }

    override fun toString(): String = "\n" +
//            this.key + "\n" +
            this.login + "\n" +
            this.token + "\n"

}