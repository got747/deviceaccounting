package com.example.goga747.deviceaccounting

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.User
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_create_new_user.*
import java.io.Serializable
import java.util.*


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CreateNewUserFragment.OnFragmentCreateNewUserListener] interface
 * to handle interaction events.
 */
class CreateNewUserFragment : Fragment() {

    private var mCreateNewUserListener: OnFragmentListener? = null
    private val LOG_TAG = "GOT"
    private val LOG_HEAD = CreateNewUserFragment::class.java.simpleName

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_new_user, container, false)
    }
    private var myRef: DatabaseReference? = null
    private var listener: ValueEventListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_login_fragment_create_user.setOnClickListener {
            if (!checkDataDisplay()) {
                Toast.makeText(context, "Not all fields are filled", Toast.LENGTH_SHORT).show()

            } else {
                val login = input_login_fragment_create_user.text.toString()
                val password = input_password_fragment_create_user.text.toString()
                Log.d("GOT", "CREATE LOGIN $login")
                Log.d("GOT", "CREATE LOGIN $password")
                val token = UUID.nameUUIDFromBytes(Base64.encode(
                        input_password_fragment_create_user.text.toString().toByteArray()
                        , Base64.DEFAULT) + login.toByteArray())
                Log.d("GOT", "CREATE LOGIN ${token.toString()}")

                myRef = FirebaseDatabase.getInstance().reference.child(ListContract.USERS_NODE)

                listener = myRef!!.orderByChild(ListContract.Companion.KeysUsersForFairBaseNode.KEY_LOGIN)
                        .equalTo(login)
                        .limitToFirst(1)
                        .addValueEventListener(object : ValueEventListener {
                            override fun onCancelled(snapshot: DatabaseError?) {
                                pushLog("onCancelled", snapshot.toString())
                            }

                            override fun onDataChange(snapshot: DataSnapshot?) {
                                pushLog("onDataChange", snapshot!!.value.toString())

                                if (snapshot!!.value != null) {
                                    Toast.makeText(context,
                                            "Login is busy"
                                            , Toast.LENGTH_SHORT).show()

                                } else {
                                    writeNewUser(token, login)
                                }
                            }

                        })

            }


        }
    }

    private fun writeNewUser(  token: UUID, login: String) {
        myRef!!.removeEventListener(listener)
        myRef!!.push().setValue(User(token.toString(), login)).addOnSuccessListener {
            Toast.makeText(context,
                    "User successfully added"
                    , Toast.LENGTH_SHORT).show()
            mCreateNewUserListener!!.closeMe(getString(R.string.key_create_new_user_fragment))
    //
        }
    }

    private fun checkDataDisplay(): Boolean {
        return if (!input_password_confirmation_fragment_create_user.text.toString()
                        .equals(input_password_fragment_create_user.text.toString())) {
            Toast.makeText(activity!!.applicationContext,
                    "Passwords do not match", Toast.LENGTH_SHORT).show()
            false
        } else {
            !((input_password_confirmation_fragment_create_user.text.toString()
                    .replace(" ", "") == "")
                    or (input_password_fragment_create_user.text.toString()
                    .replace(" ", "") == "")
                    or (input_password_confirmation_fragment_create_user.text.toString()
                    .replace(" ", "") == ""))
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            mCreateNewUserListener = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentListener " +
                    "for CreateNewUserFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()

        this.mCreateNewUserListener!!.showAppBar(true)
        mCreateNewUserListener = null
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }


}// Required empty public constructor
