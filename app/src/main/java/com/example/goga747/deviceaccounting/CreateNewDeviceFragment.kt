package com.example.goga747.deviceaccounting

import android.content.Context
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.Device
import com.example.goga747.deviceaccounting.model.DeviceTakenDevice
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_create_new_device.*
import java.io.Serializable


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [CreateNewDeviceFragment.OnFragmentCreateNewDeviceListener] interface
 * to handle interaction events.
 */
class CreateNewDeviceFragment : Fragment() {


    private val LOG_TAG = "GOT"
    private val LOG_HEAD = CreateNewDeviceFragment::class.java.simpleName
    private var mListenerCreateNewDevice: OnFragmentCreateNewDeviceListener? = null
    private lateinit var myRefDeviceNode: DatabaseReference

    private var imageDeviceUri: Uri = Uri.EMPTY

    interface OnFragmentCreateNewDeviceListener : OnFragmentListener {

        fun chooseImageManager()
    }

    private var addValueEventListener: ValueEventListener? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (this.arguments != null && this.arguments!!.containsKey(getString(R.string.key_bundle_new_device_record))) {
            val deviceJson = this.arguments!!.getString((getString(R.string.key_bundle_new_device_record)))
            pushLog("onViewCreated", deviceJson)
            val newDevice: Device = Gson().fromJson(deviceJson, Device::class.java)
            if (this.arguments!!.containsKey(getString(R.string.key_bundle_uri_image_device))) {
                this.imageDeviceUri = Uri.parse(this.arguments!!.getString(getString(R.string.key_bundle_uri_image_device), ""))
            }
            displayData(newDevice)
        } else {
            pushLog("onViewCreated ", " IS not defined key_bundle_new_device_record")
            mListenerCreateNewDevice!!.closeMe(getString(R.string.key_create_new_device_fragment))
        }
        image_view_create_new_device_fragment.setOnClickListener({
            mListenerCreateNewDevice!!.chooseImageManager()
        })
        btn_ok_create_new_device_fragment.setOnClickListener(View.OnClickListener {
            val newDevice: Device = buildDataForWriteInDB()
            this.myRefDeviceNode = FirebaseDatabase.getInstance().reference.child(ListContract.DEVICES_NODE)
            addValueEventListener = this.myRefDeviceNode
                    .orderByChild(ListContract.Companion.KeysDevicesForFairBaseNode.KEY_EXTERNAL_KEY)
                    .equalTo(newDevice.externalKey)
                    .addValueEventListener(object : ValueEventListener {
                        override fun onDataChange(snapshot: DataSnapshot?) {
                            pushLog("onDataChange", snapshot.toString())
                            if (snapshot!!.value != null) {
                                Toast.makeText(activity!!.applicationContext
                                        , "The entry can not be created because the device " +
                                        "with the key = ${newDevice.externalKey} already exists"
                                        , Toast.LENGTH_SHORT).show()
                                this@CreateNewDeviceFragment
                                        .mListenerCreateNewDevice!!
                                        .closeMe(getString(R.string.key_create_new_device_fragment))
                            } else {
                                createRecordNewDevice(newDevice)
                            }
                        }

                        override fun onCancelled(error: DatabaseError?) {
                            pushLog("onCancelled", error.toString())
                        }
                    })
        })
    }

    private fun createRecordNewDevice(newDevice: Device) {
        this.myRefDeviceNode.removeEventListener(this.addValueEventListener)
        val idCreatedDevice = this.myRefDeviceNode.push().key
        if (isImg()) {
            val photoRef: StorageReference = FirebaseStorage.getInstance()
                    .getReference(ListContract.STORAGE_DEVICE_PHOTOS).child(idCreatedDevice)
            photoRef.putFile(this.imageDeviceUri)
                    .addOnSuccessListener { taskSnapshot ->
                        taskSnapshot.downloadUrl.toString()
                        newDevice.uriImage = taskSnapshot.downloadUrl.toString()
                        myRefDeviceNode.child(idCreatedDevice).setValue(newDevice).addOnSuccessListener {
                            pushLog("addOnSuccessListener", taskSnapshot.toString())
                            addRecordDeviceInTakenDevicesNode(idCreatedDevice, DeviceTakenDevice(
                                    newDevice.model, newDevice.uriImage, newDevice.externalKey
                            ))
                        }
                    }
                    .addOnFailureListener { exception ->
                        pushLog("createRecordNewDevice  " +
                                "isImg() addOnFailureListener", exception.message.toString())
                        Toast.makeText(activity!!.applicationContext, "Device can" +
                                " not be created", Toast.LENGTH_SHORT).show()
                        this.mListenerCreateNewDevice!!
                                .closeMe(getString(R.string.key_create_new_device_fragment))
                    }
        } else {
            myRefDeviceNode.child(idCreatedDevice).setValue(newDevice).addOnSuccessListener {
                addRecordDeviceInTakenDevicesNode(idCreatedDevice, DeviceTakenDevice(
                        newDevice.model, "", newDevice.externalKey
                ))
                Toast.makeText(activity!!.applicationContext, "Device successfully added"
                        , Toast.LENGTH_SHORT).show()
            }.addOnFailureListener { exception ->
                pushLog("createRecordNewDevice " +
                        "isImg() else addOnFailureListener ", exception.message.toString())
                Toast.makeText(activity!!.applicationContext, "Device can not " +
                        "be created", Toast.LENGTH_SHORT).show()
                this.mListenerCreateNewDevice!!
                        .closeMe(getString(R.string.key_create_new_device_fragment))
            }
        }
    }

    private fun addRecordDeviceInTakenDevicesNode(idCreatedDevice: String?, deviceTakenDevice: DeviceTakenDevice) {
        val ref = FirebaseDatabase.getInstance().reference.child(ListContract.TAKEN_DEVICES_NODE
                + getString(R.string.tag_separate_query_firebase) + idCreatedDevice)
        ref.child(ListContract.SUB_DEVICE_NODE).setValue(deviceTakenDevice)
                .addOnSuccessListener {
                    Toast.makeText(activity!!.applicationContext, "The device record " +
                            "was successfully created"
                            , Toast.LENGTH_SHORT).show()
                    this.mListenerCreateNewDevice!!
                            .closeMe(getString(R.string.key_create_new_device_fragment))
                }
                .addOnFailureListener { exception ->
                    Toast.makeText(activity!!.applicationContext, "Device can not " +
                            "be created", Toast.LENGTH_SHORT).show()
                    pushLog("addRecordDeviceInTakenDevicesNode addOnFailureListener "
                            , exception.message.toString())
                    myRefDeviceNode.child(idCreatedDevice).removeValue()
                    this.mListenerCreateNewDevice!!
                            .closeMe(getString(R.string.key_create_new_device_fragment))
                }
    }

    private fun buildDataForWriteInDB(): Device {
        val modelField = text_model_create_new_device_fragment.text.toString()
        val osField = text_os_create_new_device_fragment.text.toString()
        val ramField = text_ram_create_new_device_fragment.text.toString()
        val screenInformation = text_screen_resolution_create_new_device_fragment
                .text
                .toString()
                .split(" ".toRegex())
        var screenResolutionField = ""
        var screenDensityField = ""
        try {
            screenResolutionField = screenInformation[0]
            screenDensityField = screenInformation[1]
        } catch (e: NullPointerException) {
            screenInformation.forEach { word ->
                screenResolutionField += "$word "
            }
            pushLog("buildDataForWriteInDB", "Fail,  could not assign" +
                    " screenResolutionField || screenDensityField $e")
        }
        val userField = text_user_create_new_device_fragment.text.toString()
        val pkField = text_pk_create_new_device_fragment.text.toString()
        return Device(modelField,
                osField,
                ramField,
                screenResolutionField,
                screenDensityField,
                userField,
                pkField,
                FirebaseAuth.getInstance().currentUser!!.uid,
                "")
    }

    private fun displayData(deviceData: Device) {

        text_model_create_new_device_fragment.text = deviceData.model
        text_os_create_new_device_fragment.text = deviceData.os
        text_ram_create_new_device_fragment.text = deviceData.ram
        text_screen_resolution_create_new_device_fragment.text =
                "${deviceData.screenDensity} ${deviceData.screenResolution}"
        text_user_create_new_device_fragment.text = deviceData.user
        text_pk_create_new_device_fragment.text = deviceData.externalKey
        if (isImg()) {

            Picasso.with(context).load(this.imageDeviceUri)
                    .placeholder(R.mipmap.gallery_colored)
                    .error(android.R.mipmap.sym_def_app_icon)
                    .config(Bitmap.Config.RGB_565)
                    .fit()
                    .centerCrop()
                    .into(image_view_create_new_device_fragment)
        }

    }

    private fun isImg() = !this.imageDeviceUri.equals(Uri.EMPTY) && this.imageDeviceUri.toString() != ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_create_new_device, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentCreateNewDeviceListener) {
            mListenerCreateNewDevice = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentCreateNewDeviceListener")
        }
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }

    override fun onDetach() {
        super.onDetach()
        this.mListenerCreateNewDevice!!.showAppBar(true)
        mListenerCreateNewDevice = null
    }

}// Required empty public constructor
