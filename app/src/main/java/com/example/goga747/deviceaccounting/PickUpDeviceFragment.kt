package com.example.goga747.deviceaccounting

import android.content.Context
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.goga747.deviceaccounting.db.DBHelper
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.Device
import com.example.goga747.deviceaccounting.model.DeviceTakenDevice
import com.example.goga747.deviceaccounting.model.User
import com.example.goga747.deviceaccounting.model.UserTakenDevice
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.database.*
import com.google.gson.Gson
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_pick_up_device.*
import java.io.Serializable

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [PickUpDeviceFragment.OnFragmentPickUpDeviceInteractionListener] interface
 * to handle interaction events.
 */
class PickUpDeviceFragment : Fragment() {

    private var mListenerPickUpDevice: OnFragmentListener? = null
    private lateinit var currentUser: User

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = PickUpDeviceFragment::class.java.simpleName

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (this.arguments != null && this.arguments!!.containsKey(getString(R.string.key_bundle_record_device))) {
            val deviceJson = this.arguments!!.getString((getString(R.string.key_bundle_record_device)))
            pushLog("onViewCreated", deviceJson)
            val device: Device = Gson().fromJson(deviceJson, Device::class.java)
            requestUriImg(device.externalKey)
            displayDataOnScreen(device)
            btn_ok_pick_up_device_fragment.setOnClickListener(View.OnClickListener {
                // узнаем ключ записи устройства
                FirebaseDatabase.getInstance().reference.child(ListContract.TAKEN_DEVICES_NODE)
                        .orderByChild(ListContract.SUB_DEVICE_NODE
                                + getString(R.string.tag_separate_query_firebase)
                                + ListContract.Companion.KeysDevicesForFairBaseNode.KEY_EXTERNAL_KEY)
                        .equalTo(device.externalKey)
                        .limitToFirst(1)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onCancelled(p0: DatabaseError?) {
                                pushLog("TAKEN_DEVICES_NODE onCancelled", p0.toString())
                            }

                            override fun onDataChange(snapshot: DataSnapshot?) {
                                pushLog("onDataChange", snapshot.toString())
                                if (snapshot!!.value != null) {
                                    var idDevice: String = ""
                                    snapshot.children.forEach {
                                        idDevice = it.key
                                    }
                                    addNewRecordInTakenDevice(idDevice)
                                } else {
                                    Toast.makeText(activity!!.applicationContext
                                            , "Recording not found, first add a device"
                                            , Toast.LENGTH_SHORT).show()

                                    prepareForClosure()
                                    mListenerPickUpDevice!!.closeMe(getString(R.string.key_pick_up_device_fragment))
                                    pushLog("Fail, device not found device.externalKey ="
                                            , device.externalKey)
                                }
                            }

                        })

            })
        }
    }

    private fun addNewRecordInTakenDevice(key: String?) {
        val myRef = FirebaseDatabase.getInstance()
                .reference.child(ListContract.TAKEN_DEVICES_NODE
                + getString(R.string.tag_separate_query_firebase)
                + key
                + getString(R.string.tag_separate_query_firebase)
                + ListContract.SUB_TAKEN_NODE)


        myRef.push().setValue(UserTakenDevice(currentUser.login
                , currentUser.key
                , System.currentTimeMillis()))
        prepareForClosure()
        mListenerPickUpDevice!!.closeMe(getString(R.string.key_pick_up_device_fragment))
    }

    private fun prepareForClosure() {
        val db: DBHelper = DBHelper.newInstance(activity!!.applicationContext
                , getString(R.string.name_date_base)
                , null
                , 1)
        db.deleteSession()
        pushLog("endWork", "endWork")

    }


    private fun displayDataOnScreen(deviceData: Device) {

        text_model_pick_up_device_fragment.text = deviceData.model
        text_os_pick_up_device_fragment.text = deviceData.os
        text_ram_pick_up_device_fragment.text = deviceData.ram
        text_screen_resolution_pick_up_device_fragment.text = "${deviceData.screenDensity} " +
                "${deviceData.screenResolution}"
        text_user_pick_up_device_fragment.text = deviceData.user
        text_pk_pick_up_device_fragment.text = deviceData.externalKey
        text_who_pick_up_device_fragment.text = this.currentUser.login

    }

    private fun requestUriImg(pk: String) {
        FirebaseDatabase.getInstance()
                .getReference(ListContract.DEVICES_NODE).child(pk)
                .addValueEventListener(object : ValueEventListener {
                    override fun onCancelled(p0: DatabaseError?) {
                        pushLog("requestUriImg onCancelled", p0.toString())
                    }

                    override fun onDataChange(dataSnapshot: DataSnapshot) {
                        pushLog("dataSnapshot.value ", dataSnapshot!!.value.toString())
                        if (dataSnapshot!!.value != null) {
                            var device = dataSnapshot.getValue(Device::class.java) as Device
                            displayImg(device.uriImage)
                        }
                    }
                })
    }

    private fun displayImg(uriImage: String) {
        val uri = Uri.parse(uriImage)
        if (isImg(uri)) {

            Picasso.with(context).load(uri)
                    .placeholder(android.R.mipmap.sym_def_app_icon)
                    .error(android.R.mipmap.sym_def_app_icon)
                    .config(Bitmap.Config.RGB_565)
                    .fit()
                    .centerCrop()
                    .into(image_view_pick_up_device_fragment)
        }
    }

    private fun isImg(uri: Uri) = uri.equals(Uri.EMPTY) && uri.toString() != ""

    private fun checkSessionUser(): Boolean {
        val db: DBHelper = DBHelper.newInstance(activity!!.applicationContext
                , getString(R.string.name_date_base)
                , null
                , 1)
        // TODO () В идеале db.getCurrentSession(db.writableDatabase).count == 1 else if count > 1 delete
        return db.getCurrentSession(db.writableDatabase).count > 0

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.fragment_pick_up_device, container, false)
        if (!checkSessionUser()) {
            mListenerPickUpDevice!!.closeMe(getString(R.string.key_pick_up_device_fragment))
        } else {
            this.currentUser = getCurrentUser()
        }
        return view
    }

    private fun getCurrentUser(): User {
        val db: DBHelper = DBHelper.newInstance(activity!!.applicationContext
                , getString(R.string.name_date_base), null, 1)
        return buildUserCurrentData(db.getCurrentSession(db.writableDatabase))
    }

    private fun buildUserCurrentData(session: Cursor): User {
        var user: String = ""
        var keyFB: String = ""
        if (session.moveToFirst()) {
            val userUserSessionColIndex = session.getColumnIndex(ListContract.Companion.UserSessionEntry.KEY_USER)
            val userKeyUserSessionColIndex = session.getColumnIndex(ListContract.Companion.UserSessionEntry.KEY_USER_KEY_FB)

            user = session.getString(userUserSessionColIndex)
            keyFB = session.getString(userKeyUserSessionColIndex)

        } else {
            pushLog(" buildUser ", "not record")
            mListenerPickUpDevice!!.closeMe(getString(R.string.key_pick_up_device_fragment))
        }

        return User(keyFB, user, "")
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentListener) {
            mListenerPickUpDevice = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentListener for" +
                    " PickUpDeviceInteractionFragment")
        }
    }

    override fun onDetach() {
        super.onDetach()
        prepareForClosure()
        mListenerPickUpDevice!!.showAppBar(true)
        mListenerPickUpDevice = null
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }


}// Required empty public constructor


