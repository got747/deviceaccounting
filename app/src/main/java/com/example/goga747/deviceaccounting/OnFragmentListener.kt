package com.example.goga747.deviceaccounting

/**
 * Created by goga747 on 20.05.2018.
 */
interface OnFragmentListener{
    fun closeMe(keyFragment: String)
    fun showAppBar(flag:Boolean)
}