package com.example.goga747.deviceaccounting

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.goga747.deviceaccounting.db.ListContract
import com.example.goga747.deviceaccounting.model.DeviceTakenDevice
import com.example.goga747.deviceaccounting.model.UserTakenDevice
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_display_list_taken_devices.*
import kotlin.collections.HashMap

class DisplayListTakenDevicesFragment : Fragment()
        , DisplayListTakenDevicesAdapter.OnAdapterDisplayListTakenDevicesListener
        , ChildEventListener {


    override fun getExternalKeySelectedDevice(externalKey: String) {
//        pushLog("getExternalKeySelectedDevice",externalKey)
        mListenerDisplayListTakenDevices!!.getExternalKeyDeviceForDisplayFullData(externalKey)
    }

    override fun onCancelled(error: DatabaseError?) {
        pushLog("onCancelled", error.toString())
    }

    override fun onChildMoved(snapshot: DataSnapshot?, p1: String?) {
        pushLog("onCancelled", snapshot.toString())
    }

    override fun onChildChanged(snapshot: DataSnapshot?, p1: String?) {
        var listRawDataTakenDevices = mutableListOf<UserTakenDevice>()
        if (snapshot!!.value != null) {
            if (snapshot.hasChild(ListContract.SUB_DEVICE_NODE)) {
                val infoDevice: DeviceTakenDevice = getDeviceTakenDeviceFromSnapshot(snapshot)
                if (this.listTakenDevices!!.any { it.keys.contains(infoDevice) }) {
                    if (snapshot.hasChild(ListContract.SUB_TAKEN_NODE)) {
                        listRawDataTakenDevices = getListRawDataTakenDevices(snapshot)
                    }

                    val recentUse = listRawDataTakenDevices
                            .maxBy { userTakenDevice -> userTakenDevice.dateAssignment }
                    if (listTakenDevices!!.any {
                                it[infoDevice]!!.dateAssignment >
                                        recentUse!!.dateAssignment
                            }) {
                        val indexRecord = getIndexTakenDeviceFromListTakenDevices(infoDevice)
                        listTakenDevices!![indexRecord] = hashMapOf(infoDevice to recentUse)
                        updateDataAdapter()
                    } else return
                } else return
            } else return
        }

    }

    override fun onChildAdded(snapshot: DataSnapshot?, p1: String?) {
        var listRawDataTakenDevices = mutableListOf<UserTakenDevice>()
        if (snapshot!!.value != null) {
            if (snapshot.hasChild(ListContract.SUB_DEVICE_NODE)) {
                val infoDevice: DeviceTakenDevice = getDeviceTakenDeviceFromSnapshot(snapshot)

                if (snapshot.hasChild(ListContract.SUB_TAKEN_NODE)) {
                    listRawDataTakenDevices = getListRawDataTakenDevices(snapshot)
                }
                val recentUse = listRawDataTakenDevices
                        .maxBy { userTakenDevice -> userTakenDevice.dateAssignment }
                if (this.listTakenDevices!!.any { it.keys.contains(infoDevice) }) {
                    val indexRecord = getIndexTakenDeviceFromListTakenDevices(infoDevice)
                    listTakenDevices!![indexRecord] = hashMapOf(infoDevice to recentUse)

                } else this.listTakenDevices?.add(hashMapOf(infoDevice to recentUse))
            } else return
            updateDataAdapter()
        }

    }

    private fun getListRawDataTakenDevices(snapshot: DataSnapshot): MutableList<UserTakenDevice> {
        val listRawDataTakenDevices1 = mutableListOf<UserTakenDevice>()

        snapshot.child(ListContract.SUB_TAKEN_NODE).children.forEach {

            listRawDataTakenDevices1.add(it.getValue(UserTakenDevice::class.java)
                    as UserTakenDevice)
        }
        return listRawDataTakenDevices1
    }

    private fun getIndexTakenDeviceFromListTakenDevices(infoDevice: DeviceTakenDevice): Int {
        var i = 0
        listTakenDevices!!.forEachIndexed { index, hashMap ->
            if (hashMap.containsKey(infoDevice)) {
                i = index
                return@forEachIndexed
            }
        }
        return i
    }

    override fun onChildRemoved(snapshot: DataSnapshot?) {
        pushLog("onChildRemoved", snapshot.toString())
        snapshot!!.children.forEach {
            if (it.key == ListContract.SUB_DEVICE_NODE) {

                val infoDevice: DeviceTakenDevice = it.getValue(DeviceTakenDevice::class.java)
                        as DeviceTakenDevice
                listTakenDevices?.removeAt(getIndexTakenDeviceFromListTakenDevices(infoDevice))
                pushLog("onChildRemoved1", listTakenDevices.toString())
                updateDataAdapter()
            }


        }
    }


    private var mListenerDisplayListTakenDevices:
            OnFragmentDisplayListTakenDevicesInteractionListener? = null
    private val LOG_TAG = "GOT"
    private val LOG_HEAD = DisplayListTakenDevicesFragment::class.java.simpleName

    private var listTakenDevices: MutableList<HashMap<DeviceTakenDevice, UserTakenDevice?>>? = null


    private var adapter: DisplayListTakenDevicesAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        this.listTakenDevices = mutableListOf()

        recycler_list_taken_devices.layoutManager = LinearLayoutManager(activity!!.applicationContext)
        adapter = DisplayListTakenDevicesAdapter(
                listTakenDevices
                , this
                , activity!!.applicationContext.resources.getStringArray(R.array.month)
                , activity!!.applicationContext)
        recycler_list_taken_devices.adapter = adapter

        requestDateServer()

    }


    private var myRef: DatabaseReference? = null

    private fun requestDateServer() {
        myRef = FirebaseDatabase.getInstance().getReference(ListContract.TAKEN_DEVICES_NODE)
        myRef!!.addChildEventListener(this)
    }


    private fun getDeviceTakenDeviceFromSnapshot(snapshot: DataSnapshot): DeviceTakenDevice {
        return snapshot.child(ListContract.SUB_DEVICE_NODE)
                .getValue(DeviceTakenDevice::class.java)
                as DeviceTakenDevice
    }

    private fun updateDataAdapter() {
        pushLog("updateDataAdapter", "updateDataAdapter")

        adapter!!.updateData(this.listTakenDevices)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_display_list_taken_devices, container, false)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is OnFragmentDisplayListTakenDevicesInteractionListener) {
            mListenerDisplayListTakenDevices = context
        } else {
            throw RuntimeException(context!!.toString() + " must implement OnFragmentDisplayListTakenDevicesInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        mListenerDisplayListTakenDevices = null
    }

    interface OnFragmentDisplayListTakenDevicesInteractionListener {
        fun getExternalKeyDeviceForDisplayFullData(key: String)
    }


    private fun pushLog(topic: String, message: Any) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }
}// Required empty public constructor
