package com.example.goga747.deviceaccounting

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.util.Log
import android.util.SparseArray
import com.example.goga747.deviceaccounting.model.Device
import com.google.android.gms.samples.vision.barcodereader.BarcodeCapture
import com.google.android.gms.samples.vision.barcodereader.BarcodeGraphic
import com.google.android.gms.vision.barcode.Barcode
import com.google.gson.Gson
import xyz.belvi.mobilevisionbarcodescanner.BarcodeRetriever

class BarcodeActivity : AppCompatActivity()//{
        , BarcodeRetriever {

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = BarcodeActivity::class.java.simpleName


    override fun onRetrieved(barcode: Barcode?) {
        processingBarcodeForRecordingNewDevice(barcode!!.displayValue)
    }

    override fun onRetrievedMultiple(closetToClick: Barcode?, barcode: MutableList<BarcodeGraphic>?) {
        runOnUiThread {
            pushLog("onRetrievedMultiple ",barcode.toString())
        }
    }


    override fun onBitmapScanned(p0: SparseArray<Barcode>?) {
        pushLog("onBitmapScanned ",p0.toString())
    }

    override fun onRetrievedFailed(p0: String?) {
        pushLog("onRetrievedFailed ",p0.toString())
    }

    override fun onPermissionRequestDenied() {
        pushLog("onPermissionRequestDenied ", "")
    }

    private fun processingBarcodeForRecordingNewDevice(barcodeString: String?) {

        pushLog("onRetrieved newDevice", barcodeString!!)
        setResult(RESULT_OK, Intent().apply { putExtra(getString(R.string.key_bundle_record_device),barcodeString) })
        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_barcode)
        displayBarcode()
    }

    private fun displayBarcode() {
        pushLog("displayBarcode","")
        val barcodeCapture = supportFragmentManager.findFragmentById(R.id.fragment_barcode_created_new_device_record) as BarcodeCapture
        barcodeCapture.setRetrieval(this)
        barcodeCapture.setShowDrawRect(true).barcodeFormat = Barcode.ALL_FORMATS

    }

    private fun pushLog(topic: String, message: Any) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }

}
