package com.example.goga747.deviceaccounting.model

/**
 * Created by goga747 on 05.05.2018.
 */
open class UserTakenDevice() {
    lateinit var login: String
    lateinit var key: String
    var dateAssignment: Long = 0

    constructor(login: String, key: String, dateAssignment: Long) : this() {
        this.login = login
        this.key = key
        this.dateAssignment = dateAssignment
    }
    override fun toString(): String = "\n"+
            this.login + "\n" +
            this.key + "\n" +
            this.dateAssignment + "\n"

}

open class DeviceTakenDevice() {
    lateinit var model: String
    lateinit var uriImage: String
    lateinit var externalKey: String

    constructor(model: String, uriImage: String, externalKey: String) : this() {
        this.model = model
        this.uriImage = uriImage
        this.externalKey = externalKey
    }

    override fun toString(): String = "\n"+
            this.model + "\n" +
            this.externalKey + "\n" +
            this.uriImage + "\n"
}

