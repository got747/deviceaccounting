package com.example.goga747.deviceaccounting.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.io.Serializable
import java.util.*

/**
 * Created by goga747 on 03.05.2018.
 */
class DBHelper(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?,
               version: Int) :
        SQLiteOpenHelper(context, name, factory, version) {

    private val LOG_TAG = "GOT"
    private val LOG_HEAD = DBHelper::class.java.simpleName

//    private

    private val TEXT_TYPE = " TEXT"
    private val INT_TYPE = " INTEGER"

    private val SQL_CREATE_TABLE_USER_SESSION = "CREATE TABLE ${ListContract.TABLE_USER_SESSION} " +
            "(key INTEGER PRIMARY KEY AUTOINCREMENT," +
            " ${ListContract.Companion.UserSessionEntry.KEY_USER} $TEXT_TYPE NOT NULL," +
            " ${ListContract.Companion.UserSessionEntry.KEY_USER_KEY_FB} $TEXT_TYPE NOT NULL," +
            " ${ListContract.Companion.UserSessionEntry.KEY_CREATED} $INT_TYPE NOT NULL);"


    companion object {
        private var instance: DBHelper? = null

        fun newInstance(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?,
                        version: Int): DBHelper {
            return if (instance == null) {
                instance = DBHelper(context, name, factory, version)
                instance!!
            } else {
                instance!!
            }
        }

    }

    override fun onCreate(sqLiteDatabase: SQLiteDatabase?) {
        sqLiteDatabase?.execSQL(SQL_CREATE_TABLE_USER_SESSION)
    }

    fun addSession(user: String, key: String) {
        val db = this.writableDatabase
        val value = ContentValues()

        value.put(ListContract.Companion.UserSessionEntry.KEY_USER, user)
        value.put(ListContract.Companion.UserSessionEntry.KEY_USER_KEY_FB, key)
        value.put(ListContract.Companion.UserSessionEntry.KEY_CREATED, System.currentTimeMillis().toInt())

        pushLog("addSession value", value.toString())

        db.insert(ListContract.TABLE_USER_SESSION, null, value)
        db.close()
    }

    fun deleteSession() {
        val db = this.writableDatabase
        val deleteSession = db.delete(ListContract.TABLE_USER_SESSION, "1", null)

        pushLog(" row delete ", deleteSession)
        db.close()
    }

    fun getCurrentSession(db: SQLiteDatabase): Cursor {
        val queryTask = db.query(ListContract.TABLE_USER_SESSION, null, null, null, null, null, null)
        pushLog(" getCurrentSession ", queryTask.toString())
        return queryTask
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase?, p1: Int, p2: Int) {
        pushLog("onUpgrade", sqLiteDatabase!!.version)
    }

    private fun pushLog(topic: String, message: Serializable) {
        Log.d(LOG_TAG, "$LOG_HEAD $topic $message")
    }
}